import { Routes,Route } from 'react-router-dom';
import Login from './pages/auth/login';
import Dashboard from './pages/master/dashboard';
import Kategoribuku from './pages//master/masterkategoribuku'
import Masterbuku from './pages/master/masterbuku'
import Transaksipeminjaman from './pages/transaksi/transaksipeminjaman'
import Transaksipengembalian from './pages/transaksi/transaksipengembalian'
import Masterpeminjam from './pages/master/masterpeminjam'


function App() {
  return (
    <div className="App">
      <Routes>
        {/* auth */}
        <Route path="/login" element={<Login/>}/>
        
        {/* master */}
        <Route path="/dashboard" element={<Dashboard/>}/>
        <Route path="/kategoribuku" element={<Kategoribuku/>}/>
        <Route path="/buku" element={<Masterbuku/>}/>
        <Route path="/peminjam" element={<Masterpeminjam/>}/>

        {/* transaksi */}
        <Route path="/transaksipeminjaman" element={<Transaksipeminjaman/>}/>
        <Route path="/transaksipengembalian" element={<Transaksipengembalian/>}/>
      </Routes>
    </div>
  );
}

export default App;
