import React, {useEffect} from 'react'
import {useNavigate} from 'react-router-dom';
import Footer from '../static/footer';
import Header from '../static/header';

function Dashboard() {

    const navigate = useNavigate();
  
    useEffect(() => {
        if(localStorage.getItem('id_petugas') === null){
            navigate('/login',{replace:true});
        }
    },[navigate]);
  
    return (
        <div>
           <Header/>
           <Footer/> 
        </div>
  )
}

export default Dashboard