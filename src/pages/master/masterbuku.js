import React,{useState,useEffect} from 'react'
import {useNavigate,Link} from 'react-router-dom'
import Footer from '../static/footer'
import Header from '../static/header'
import axios from 'axios'
import swal from 'sweetalert'
import Select from 'react-select'

function Masterbuku() {

  const navigate = useNavigate();

  const [data, setData] = useState([]);
  
  const [onChangeCoverBuku, setOnChangeCoverBuku] = useState(null);
  const [onChangeJudul, setOnChangeJudul] = useState("");
  const [onChangePengarang, setOnChangePengarang] = useState("");
  const [onChangeTahunTerbit, setOnChangeTahunTerbit] = useState(0);
  const [onChangeStok, setOnChangeStok] = useState(0);
  const [onChangeIdKategoriBuku, setOnChangeIdKategoriBuku] = useState(0);

  const [onChangeCoverBukuEdit, setOnChangeCoverBukuEdit] = useState(null);
  const [onChangeCoverBukuEditOld, setOnChangeCoverBukuEditOld] = useState(null);
  const [onChangeJudulEdit, setOnChangeJudulEdit] = useState("");
  const [onChangePengarangEdit, setOnChangePengarangEdit] = useState("");
  const [onChangeTahunTerbitEdit, setOnChangeTahunTerbitEdit] = useState(0);
  const [onChangeStokEdit, setOnChangeStokEdit] = useState(0);
  const [onChangeIdKategoriBukuEdit, setOnChangeIdKategoriBukuEdit] = useState(0);

  const [dataKategoriBuku, setDataKategoriBuku] = useState([]);

  const [dataKategoriBukuEdit, setDataKategoriBukuEdit] = useState([]);

  const [openModalEdit, setModalEdit] = useState(false);
  const [idBuku, setIdBuku] = useState(0);
  const [idBukuEdit, setIdBukuEdit] = useState(0);

  useEffect(() => {
    if(localStorage.getItem('id_petugas') === null){
        navigate('/login',{replace:true});
    }
  },[navigate]);

  const loadData =()=>{
    axios.get(`http://localhost:80/android/index.php/perpusreact/list_buku`)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          setData(response.data.data);
        }else{

        }
      }else{
        swal("Error","Gagal Get Data Kategori","error");
      }
    })
  }

    // kategori buku
    useEffect(()=>{
      axios
        .get(`http://localhost:80/android/index.php/perpusreact/list_kategori_buku`)
        .then((response)=>{
          if(response.status === 200){
            if(response.data.status === 1){
              const data = response.data;
              const options = data?.data.map((e)=>({
                "value":e.id,
                "label":e.nama_kategori
              }));
      
              setDataKategoriBuku(options);
            }
          }
        });
    },[navigate])
    // kategori buku

  useEffect(()=>{
    if(localStorage.getItem('id') === ""){
      navigate('/login',{replace:true})
    }
    loadData()
  },[navigate]);

  const handleChangeJudul=(event) =>{
    let judul = event.target.value;
    setOnChangeJudul(judul);
  }

  const handleChangeJudulEdit=(event) =>{
    let judul = event.target.value;
    setOnChangeJudulEdit(judul);
  }

  const handleChangePengarang=(event) =>{
    let pengarang = event.target.value;
    setOnChangePengarang(pengarang);
  }

  const handleChangePengarangEdit=(event) =>{
    let pengarang = event.target.value;
    setOnChangePengarangEdit(pengarang);
  }

  const handleChangeTahunTerbit=(event) =>{
    let tahun_terbit = event.target.value;
    setOnChangeTahunTerbit(tahun_terbit);
  }

  const handleChangeTahunTerbitEdit=(event) =>{
    let tahun_terbit = event.target.value;
    setOnChangeTahunTerbitEdit(tahun_terbit);
  }

  const handleChangeStok=(event) =>{
    let stok = event.target.value;
    setOnChangeStok(stok);
  }

  const handleChangeStokEdit=(event) =>{
    let stok = event.target.value;
    setOnChangeStokEdit(stok);
  }

  const handleKategori=(event)=>{
    const get_id_kategori = event.value;
    setOnChangeIdKategoriBuku(get_id_kategori);
    // console.log(get_id_kategori);
  }

  const handleChangeKategoriEdit=(event)=>{
    setOnChangeIdKategoriBukuEdit(event.value);
  }

  const handleSubmit=()=>{
    // console.log(onChangeIdKategoriBuku.value);

    const formData = new FormData();
    formData.append("file", onChangeCoverBuku);
    formData.append("judul", onChangeJudul);
    formData.append("pengarang", onChangePengarang);
    formData.append("tahun_terbit", onChangeTahunTerbit);
    formData.append("stok_buku", onChangeStok);
    formData.append("id_kategori",onChangeIdKategoriBuku.value);
    
    axios.post(`http://localhost:80/android/index.php/perpusreact/add_buku`,formData,{
      headers:{
        'Content-Type': 'multipart/form-data'
      }
    })
    .then((response)=>{
      if(response.status === 200){
        // console.log(response.data.data);
        if(response.data.status === 1){
          // console.log(response.data.data);
          loadData()
        }else{
          swal("Error",""+response.data.message,"error")
        }
      }else{
        swal("Error","Insert data gagal","error")
      }
    })
  }

  const handleDelete=(e, id)=>{
    e.preventDefault();

    const deleteData = {
      id:id,
    }

    axios.post(`http://localhost:80/android/index.php/perpusreact/delete_buku`,deleteData)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          loadData();
        }else{
          swal("Error",""+response.data.message,"error");
        }
      }else{
        swal("Error","Insert data gagal","error");
      }
    })
  }

  const handleCloseModalEdit=()=>{
    setModalEdit(false);
  }

  useEffect(() => {
   
    axios
      .post(`http://localhost:80/android/index.php/perpusreact/list_kategori_buku`)
      .then((response) => {
        if(response.status === 200){
          if(response.data.status === 1){
            const data = response.data;
            const options = data.data?.map((e)=>({
              "value":e.id, "label":e.nama_kategori
            }))
            setDataKategoriBukuEdit(options);
          }
        }
      })
  },[])

  const handleGetEdit=(e, id)=>{
    e.preventDefault();
  
    const getData = {
      id:id
    };
    axios.post(`http://localhost:80/android/index.php/perpusreact/get_buku_by_id`,getData)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          setOnChangeCoverBukuEditOld(response.data.data[0].cover_buku);
          setOnChangeJudulEdit(response.data.data[0].judul);
          setOnChangePengarangEdit(response.data.data[0].pengarang);
          setOnChangeTahunTerbitEdit(response.data.data[0].tahun_terbit);
          setOnChangeStokEdit(response.data.data[0].stok_buku);
          setOnChangeIdKategoriBukuEdit(response.data.data[0].id_kategori);
          // document.getElementById("nama_kategori_edit").innerHTML = response.data.data[0].nama_kategori;
          setIdBukuEdit(id);
          setModalEdit(true);
        }else{
          swal("Error",""+response.data.message,"error");
        }
      }else{
        swal("Error","Insert data gagal","error");
      }
    })
  }

  const handleProsesEdit=()=>{
    const formData = new FormData();

    formData.append("cover_buku", onChangeCoverBukuEditOld);
    formData.append("file", onChangeCoverBukuEdit);
    formData.append("judul", onChangeJudulEdit);
    formData.append("pengarang", onChangePengarangEdit);
    formData.append("tahun_terbit", onChangeTahunTerbitEdit);
    formData.append("stok_buku", onChangeStokEdit);
    formData.append("id_kategori",onChangeIdKategoriBukuEdit);
    formData.append("id",idBukuEdit);

    axios.post(`http://localhost:80/android/index.php/perpusreact/edit_buku`,formData,{
      headers:{
        'Content-Type': 'multipart/form-data'
      }
    })
    .then((response)=>{
      if(response.status === 200){
        // console.log(response.data.data);
        if(response.data.status === 1){
          // console.log(response.data.data);
          loadData();
          setModalEdit(false);
        }else{
          swal("Error",""+response.data.message,"error")
        }
      }else{
        swal("Error","Insert data gagal","error")
      }
    })


  }


  return (
    <div>
      <Header/>
      {/* content start */}
      <div className="container-fluid my-5">
        <div className="row">
          <div className="col-md-12">
            {/* card */}
            <div className="card">
              <div className="card-header text-left">
                <h5>Data Master Buku</h5>
              </div>
              <div className="card-body text-left">
                {/* <Link to="/add"> */}
                  <button className="btn btn-info btn-md my-3"
                  data-toggle="modal" data-target="#ModalAddData">Add Data</button>
                {/* </Link> */}
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Cover Buku</th>
                      <th scope="col">Judul</th>
                      <th scope="col">Pengarang</th>
                      <th scope="col">Tahun Terbit</th>
                      <th scope="col">Stok</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data?.map((e, i) => {
                      return (
                        <tr key={e.id}>
                          <th scope="row" key={e}>{i+1}</th>
                          <td><img src={`http://localhost:80/android/assets/perpusimage/${e.cover_buku}`} width="90px" 
                          height="90px" alt='hello'/></td>
                          <td>{e.judul}</td>
                          <td>{e.pengarang}</td>
                          <td>{e.tahun_terbit}</td>
                          <td>{e.stok_buku}</td>
                          <td>
                            {/* <Link to={`/edit/${e.id}`}> */}
                              <button className="btn btn-warning btn-xs mx-3" onClick={(event)=>handleGetEdit(event, e.id)}>
                                Edit
                              </button>
                            {/* </Link> */}
                            <button className="btn btn-danger btn-xs" onClick={(event)=>handleDelete(event, e.id)}>
                              Delete
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            {/* card */}
          </div>
        </div>
      </div>
      {/* content end */}
      {/* <Footer/> */}

       {/* modal add start */}
       <div className="modal fade" id="ModalAddData" tabindex="-1"
       role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">Form Add Data</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                    {onChangeCoverBuku && (
                      <div>
                      <img alt="not fount" width={"250px"} src={URL.createObjectURL(onChangeCoverBuku)} />
                      <br />
                      <button onClick={()=>setOnChangeCoverBuku(null)} className="btn btn-sm btn-danger">Remove</button>
                      </div>
                    )}
                    <br />
                    <label>Cover Buku</label>
                    <input
                    type="file"
                    onChange={(event) => {
                      console.log(event.target.files[0]);
                      setOnChangeCoverBuku(event.target.files[0]);
                    }}
                    className="form-control"
                    id="cover_buku"
                    placeholder="Pilih Cover Buku"
                    />
                </div>

                <div className="form-group">
                    <label>Judul Buku</label>
                    <input
                    type="text"
                    onChange={handleChangeJudul}
                    value={onChangeJudul}
                    className="form-control"
                    id="nama_buku"
                    placeholder="Masukkan Nama Buku"
                    />
                </div>

                <div className="form-group">
                    <label>Pengarang</label>
                    <input
                    type="text"
                    onChange={handleChangePengarang}
                    value={onChangePengarang}
                    className="form-control"
                    id="pengarang"
                    placeholder="Masukkan Pengarang"
                    />
                </div>

                <div className="form-group">
                    <label>Tahun Terbit</label>
                    <input
                    type="number"
                    onChange={handleChangeTahunTerbit}
                    value={onChangeTahunTerbit}
                    className="form-control"
                    id="tahun_terbit"
                    placeholder="Masukkan Tahun Terbit"
                    />
                </div>

                <div className="form-group">
                    <label>Stok</label>
                    <input
                    type="number"
                    onChange={handleChangeStok}
                    value={onChangeStok}
                    className="form-control"
                    id="stok_buku"
                    placeholder="Masukkan Stok"
                    />
                </div>

                <div className="form-group">
                  <label>Kategori Buku</label>
                  <Select 
                    value={onChangeIdKategoriBuku}
                    options={dataKategoriBuku} 
                    id="id_kategori_buku" 
                    onChange={handleKategori}
                    placeholder="Pilih Kategori Buku ..."
                  />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" onClick={handleSubmit}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
      {/* modal add end */}

      {/* modal edit start */}
      <div className={(openModalEdit) ? "modal fade show" : "modal fade"} 
        style={(openModalEdit) ? {display:"block"} : {display:"none"}}
         id="ModalEditData" tabindex="-1"
       role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">Form Edit Data</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                    {onChangeCoverBukuEdit && (
                      <div>
                      <img alt="not fount" width={"100px"} height={"50px"} src={URL.createObjectURL(onChangeCoverBukuEdit)} />
                      <br />
                      <button onClick={()=>setOnChangeCoverBukuEdit(null)} className="btn btn-sm btn-danger">Remove</button>
                      </div>
                    )}
                    <br />
                    <label>Cover Buku</label>
                    <input
                    type="file"
                    onChange={(event) => {
                      console.log(event.target.files[0]);
                      setOnChangeCoverBukuEdit(event.target.files[0]);
                    }}
                    className="form-control"
                    id="cover_buku"
                    placeholder="Pilih Cover Buku"
                    />
                </div>

                <div className='form-group'>
                <img src={`http://localhost:80/android/assets/perpusimage/${onChangeCoverBukuEditOld}`} width="90px" 
                          height="90px" alt='hello'/>
                </div>

                <div className="form-group">
                    <label>Judul Buku</label>
                    <input
                    type="text"
                    onChange={handleChangeJudulEdit}
                    defaultValue={onChangeJudulEdit}
                    className="form-control"
                    id="nama_buku"
                    placeholder="Masukkan Nama Buku"
                    />
                </div>

                <div className="form-group">
                    <label>Pengarang</label>
                    <input
                    type="text"
                    defaultValue={onChangePengarangEdit}
                    onChange={handleChangePengarangEdit}
                    className="form-control"
                    id="pengarang"
                    placeholder="Masukkan Pengarang"
                    />
                </div>

                <div className="form-group">
                    <label>Tahun Terbit</label>
                    <input
                    type="number"
                    onChange={handleChangeTahunTerbitEdit}
                    value={onChangeTahunTerbitEdit}
                    className="form-control"
                    id="tahun_terbit"
                    placeholder="Masukkan Tahun Terbit"
                    />
                </div>

                <div className="form-group">
                    <label>Stok</label>
                    <input
                    type="number"
                    value={onChangeStokEdit}
                    onChange={handleChangeStokEdit}
                    className="form-control"
                    id="stok_buku"
                    placeholder="Masukkan Stok"
                    />
                </div>

                <div className="form-group">
                  <label>Kategori Buku</label>
                  <Select 
                        options={dataKategoriBukuEdit}
                        onChange={handleChangeKategoriEdit}
                        value={ (dataKategoriBukuEdit.filter(obj => obj.value === onChangeIdKategoriBukuEdit))}
                        id="id_prodi"
                        placeholder="Pilih Prodi ..."
                      />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" onClick={handleProsesEdit}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
      {/* modal edit end */}

    </div>
  )
}

export default Masterbuku