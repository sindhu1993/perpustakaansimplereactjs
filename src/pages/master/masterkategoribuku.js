import React,{useState,useEffect} from 'react'
import {useNavigate,Link} from 'react-router-dom'
import Footer from '../static/footer'
import Header from '../static/header'
import axios from 'axios'
import swal from 'sweetalert'

function Masterkategoribuku() {

  const navigate = useNavigate();

  const [data, setData] = useState([]);
  const [onChangeNamaKategori, setOnChangeNamaKategori] = useState("");

  const [openModalEdit, setModalEdit] = useState(false);
  const [onChangeNamaKategoriEdit, setOnChangeNamaKategoriEdit] = useState("");
  const [idKategori, setIdKategori] = useState(0);

  useEffect(() => {
    if(localStorage.getItem('id_petugas') === null){
        navigate('/login',{replace:true});
    }
  },[navigate]);

  const loadData =()=>{
    axios.get(`http://localhost:80/android/index.php/perpusreact/list_kategori_buku`)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          setData(response.data.data);
        }else{

        }
      }else{
        swal("Error","Gagal Get Data Kategori","error");
      }
    })
  }

  const handleChangeNamaKategori=(event) =>{
    let nama_kategori = event.target.value;
    setOnChangeNamaKategori(nama_kategori);
  }

  const handleSubmit=(event) =>{
    event.preventDefault();
    const dataInsert = {
      nama_kategori:onChangeNamaKategori
    };

    axios.post(`http://localhost:80/android/index.php/perpusreact/add_kategori`,dataInsert)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          loadData();
        }else{
          swal("Error",""+response.data.message,"error")
        }
      }else{
        swal("Error","Insert data gagal","error")
      }
    })
  }

  const handleCloseModalEdit=()=>{
    setModalEdit(false);
  }

  const handleGetEdit=(e, id)=>{
    e.preventDefault();
  
    const getData = {
      id:id
    };
    axios.post(`http://localhost:80/android/index.php/perpusreact/get_kategori_by_id`,getData)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          setOnChangeNamaKategoriEdit(response.data.data[0].nama_kategori);
          // document.getElementById("nama_kategori_edit").innerHTML = response.data.data[0].nama_kategori;
          setIdKategori(id);
          setModalEdit(true);
        }else{
          swal("Error",""+response.data.message,"error");
        }
      }else{
        swal("Error","Insert data gagal","error");
      }
    })
  }

  const handleProsesEdit = (event)=>{
    event.preventDefault();

    const edit = {
      nama_kategori:document.getElementById("nama_kategori_edit").value,
      id:idKategori
    }

    // console.log(edit);

    axios.post(`http://localhost:80/android/index.php/perpusreact/proses_edit_kategori_buku`,edit)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          loadData();
          setModalEdit(false);
        }else{
          swal("Error",""+response.data.message,"error");
        }
      }else{
        swal("Error","Insert data gagal","error");
      }
    })
  }

  const handleDelete=(e, id)=>{
    e.preventDefault();

    const deleteData = {
      id:id,
    }

    axios.post(`http://localhost:80/android/index.php/perpusreact/delete_kategori`,deleteData)
    .then((response)=>{
      if(response.status === 200){
        if(response.data.status === 1){
          loadData();
        }else{
          swal("Error",""+response.data.message,"error");
        }
      }else{
        swal("Error","Insert data gagal","error");
      }
    })
  }

  useEffect(()=>{
    if(localStorage.getItem('id') === ""){
      navigate('/login',{replace:true})
    }
    loadData()
  },[navigate]);

  return (
    <div>
      <Header/>
      {/* content start */}
      <div className="container-fluid my-5">
        <div className="row">
          <div className="col-md-12">
            {/* card */}
            <div className="card">
              <div className="card-header text-left">
                <h5>Data Master Kategori</h5>
              </div>
              <div className="card-body text-left">
                {/* <Link to="/add"> */}
                  <button className="btn btn-info btn-md my-3"
                  data-toggle="modal" data-target="#ModalAddData">Add Data</button>
                {/* </Link> */}
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Nama Kategori</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data?.map((e, i) => {
                      return (
                        <tr key={e.id}>
                          <th scope="row" key={e}>{i+1}</th>
                          <td>{e.nama_kategori}</td>
                          <td>
                            {/* <Link to={`/edit/${e.id}`}> */}
                              <button className="btn btn-warning btn-xs mx-3" onClick={(event)=>handleGetEdit(event, e.id)}>
                                Edit
                              </button>
                            {/* </Link> */}
                            <button className="btn btn-danger btn-xs" onClick={(event)=>handleDelete(event, e.id)}>
                              Delete
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
            {/* card */}
          </div>
        </div>
      </div>
      {/* content end */}
      <Footer/>

      {/* modal add start */}
      <div className="modal fade" id="ModalAddData" tabindex="-1"
       role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">Form Add Data</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                    <label>Nama Kategori</label>
                    <input
                    type="username"
                    onChange={handleChangeNamaKategori}
                    value={onChangeNamaKategori}
                    className="form-control"
                    id="nama_kategori"
                    placeholder="Masukkan Nama Kategori"
                    />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" onClick={handleSubmit}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
      {/* modal add end */}

      {/* modal edit start */}
      <div className={(openModalEdit) ? "modal fade show" : "modal fade"} 
      style={(openModalEdit) ? {display:"block"} : {display:"none"}}
      id="ModalEditData" tabindex="-1"
       role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">Form Edit Data</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                    <label>Nama Kategori</label>
                    <input type="hidden" id='id' value={idKategori}/>
                    <input
                    type="text"
                    defaultValue={onChangeNamaKategoriEdit}
                    className="form-control"
                    id="nama_kategori_edit"
                    placeholder="Masukkan Nama Kategori"
                    />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={handleCloseModalEdit} data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" onClick={handleProsesEdit}>Simpan</button>
            </div>
          </div>
        </div>
      </div>
      {/* modal edit end */}

    </div>
  )
}

export default Masterkategoribuku