import React from 'react';
import {Link} from 'react-router-dom'

function Header () {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link to="/"><a className="navbar-brand" href="/">PerpusReact</a></Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Master
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link to="/kategoribuku"><a className="dropdown-item" href="#">Kategori Buku</a></Link>
                <Link to="/buku"><a className="dropdown-item" href="#">Buku</a></Link>
                <Link to="/peminjam"><a className="dropdown-item" href="#">Peminjam</a></Link>
              </div>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Transaksi
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="/transaksipeminjaman"><a className="dropdown-item" href="#">Peminjaman</a></Link>
              <Link to="/transaksipengembalian"> <a className="dropdown-item" href="#">Pengembalian</a></Link> 
              </div>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <p className="my-2 mx-2">Welcome, {localStorage.getItem('username')}</p>
            <button
              className="btn btn-sm btn-outline-dark my-2 my-sm-0"
              type="submit"
            >
              Logout
            </button>
          </form>
        </div>
      </nav>
    </div>
  );
}

export default Header;
