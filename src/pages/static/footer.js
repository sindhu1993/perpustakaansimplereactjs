import React from 'react';
import './footer.css';

function Footer () {
  return (
    <div>
      <footer className="footer">
        <div className="container">
          <div className="box">
            <h5 style={{marginTop:'15px'}}>CopyRight @2022. All rights reserved</h5>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
