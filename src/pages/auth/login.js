import axios from 'axios';
import React,{useState,useEffect} from 'react';
import swal from 'sweetalert';
import {useNavigate} from 'react-router-dom'


function Login () {

  const [onChangeUsername, setOnChangeUsername] = useState("");
  const [onChangePassword, setOnChangePassword] = useState("");

  const navigate = useNavigate();

//   useEffect(()=>{
//     if(localStorage.getItem('id') !== null){
//         navigate('/dashboard',{replace:true})
//     }
//   });

  const handleChangeUsername = (event) => {
    let username = event.target.value;
    setOnChangeUsername(username);
  }

  const handleChangePassword = (event) => {
    let password = event.target.value;
    setOnChangePassword(password);
  }

  const handleLogin=()=>{
    const dataLogin = {
        username:onChangeUsername,
        password:onChangePassword
    }

    axios
        .post(`http://localhost:80/android/index.php/perpusreact/login_perpus`,dataLogin)
        .then((response)=>{
            if(response.status === 200){
                if(response.data.status === 1){
                    // console.log(response.data.data[0].username);
                    localStorage.setItem('username',response.data.data[0].username);
                    localStorage.setItem('id_petugas',response.data.data[0].id);
                    navigate('/dashboard',{replace:true});
                }
                else{
                    swal("Warning",""+response.data.message,"warning");
                }
            }
            else{
                swal("Error","Gagal Login","error");
            }
        })
    } 

    return (
        <div className="my-4 mx-4">
            <div className="container">
                <div className="card">
                <div className="card-header">
                    <h1>Login Page</h1>
                </div>
                <div className="card-body">
                    <form>
                    <div className="form-group">
                        <label>Username</label>
                        <input
                        type="username"
                        onChange={handleChangeUsername}
                        value={onChangeUsername}
                        className="form-control"
                        id="username"
                        placeholder="Enter username"
                        />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input
                        type="password"
                        className="form-control"
                        onChange={handleChangePassword}
                        value={onChangePassword}
                        id="password"
                        placeholder="Password"
                        />
                    </div>
                    <button type="button" className="btn btn-primary" onClick={handleLogin}>Login</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
